import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "./guards/auth.guard";


const routes: Routes = [
  {path : '' , redirectTo : '/sign-in', pathMatch: "full"},
  {path : 'sign-in' , loadChildren : './auth/auth.module#AuthModule'},
  {path : 'dashboard' ,canActivate : [AuthGuard], loadChildren : './dashboard/dashboard.module#DashboardModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
