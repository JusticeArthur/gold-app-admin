import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {UiService} from "../../services/ui.service";
import {User} from "../../models/user.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  processing = false;
  signInForm : FormGroup;
  constructor(private authService : AuthService,
              private  uiService : UiService,private router : Router) { }

  ngOnInit() {
   // this.authService.autoSignIn();
    this.signInForm = new FormGroup({
      username : new FormControl(null,{updateOn : "change" , validators : [Validators.required]}),
      password : new FormControl(null,{updateOn : "change" , validators : [Validators.required]}),
    })
  }

  signIn(){
    let formData = new FormData();
    formData.append('username',this.signInForm.get('username').value);
    formData.append('password',this.signInForm.get('password').value);
    this.processing = true;
    this.authService.signIn(formData).subscribe(resp => {
      localStorage.setItem('userInfo',JSON.stringify(resp.data));
      this.authService.createNewUserInstance();
      this.router.navigate(['/dashboard']).then(resp => {
        this.uiService.showSuccess('signed in successfully');
      })
    },error => {
      this.uiService.showError(error.error);
      this.processing = false;
    })
  }

}
