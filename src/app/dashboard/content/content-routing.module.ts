import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AuthGuard} from "../../guards/auth.guard";
import {QuestionsComponent} from "./questions/questions.component";
import {AddQuestionComponent} from "./questions/add-question/add-question.component";
import {PushContentComponent} from "./push-content/push-content.component";



const routes : Routes = [
  {path : '' , canActivate : [AuthGuard],canActivateChild : [AuthGuard] , children : [
      {path: '', component: QuestionsComponent},
      {path : 'add-question' ,component : AddQuestionComponent},
      {path : 'push' ,component : PushContentComponent},
    ]
  }
];
@NgModule({
  imports : [RouterModule.forChild(routes)],
  exports : [RouterModule]
})
export class ContentRoutingModule {

}
