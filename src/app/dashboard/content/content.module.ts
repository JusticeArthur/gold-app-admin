import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared/shared.module";
import {ContentRoutingModule} from "./content-routing.module";
import {AddQuestionComponent} from "./questions/add-question/add-question.component";
import {QuestionsComponent} from "./questions/questions.component";
import {FileDropDirective} from "../../directives/file-drop.directive";
import { PushContentComponent } from './push-content/push-content.component';
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    QuestionsComponent,
    AddQuestionComponent,
    FileDropDirective,
    PushContentComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ContentRoutingModule,
    MatButtonModule,

  ]
})
export class ContentModule{ }
