import { Component, OnInit } from '@angular/core';
import {CdkDragDrop,moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Question} from "../../../models/question.model";
import {ContentService} from "../../../services/dashboard/content.service";
import {Paginatable} from "../../../services/env.service";
import {PillarService} from "../../../services/dashboard/settings/pillar.service";
import {Pillar, Topic} from "../../../services/auth.service";
import {UiService} from "../../../services/ui.service";

@Component({
  selector: 'app-push-content',
  templateUrl: './push-content.component.html',
  styleUrls: ['./push-content.component.scss']
})
export class PushContentComponent implements OnInit {

  title = 'Drag & Drop questions';

  questions : Question[] = [];
  content : Question[] = [];
  loadingMore = false;
  pillars : Pillar[] = [];
  topics : Topic[] = [];
  loadingFullContent = true;
  questionPage : Paginatable<Question>;
  contentPage : Paginatable<Question>;
  loadingQuestions = false;
  currentTopic = 0;
  constructor(private contentService : ContentService,private pillarService : PillarService,private uiService : UiService) { }

  ngOnInit() {
    this.pillarService.fetchPillars().subscribe(resp => {
      // @ts-ignore
      this.pillars = <Pillar[]>resp.data;
    })
  }

  resetContent(){
    this.questions = [];
    this.content = [];
    this.loadingFullContent = true;
  }

  onPillarChanged(value){
    this.resetContent();
    this.topics = [];
    this.contentService.pillarTopics(value).subscribe(resp => {
      this.topics = resp.data;
    })
  }
  onTopicChanged(value){
    this.resetContent();
    this.currentTopic = value;
    this.loadingQuestions = true;
    this.contentService.fetchQuestions(value).subscribe(page => {
      if(page){
        this.questionPage = page.data.questions;
        this.questions = this.questionPage.data;

        //
        this.contentPage =  page.data.content;
        this.content = this.contentPage.data;
        this.loadingFullContent = false;
      }
      this.loadingQuestions = false;
    },error => {
      this.loadingQuestions = false;
    });
  }

  onDrop(event: CdkDragDrop<Question[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex, event.currentIndex);
    }
  }

  loadMoreQuestions(){
    this.loadingMore = true;
    this.contentService.fetchQuestions(this.currentTopic,this.questionPage.next_page_url).subscribe(page => {
      this.loadingMore = false;
      if(page){
        this.questionPage = page.data.questions;
        this.questions = [...this.questions,...this.questionPage.data]
      }
    },error => {
      this.loadingMore = false;
    })
  }

  get pushEnabled(){
    return this.content.length > 0;
  }

  onPushContent(){
    let formData = new FormData();
    let content = this.content.map(item => {
      return item.id
    });
    formData.append('content',JSON.stringify(content));
    formData.append('topic_id',this.currentTopic.toString());
    this.contentService.pushContent(formData).subscribe(resp => {
      this.uiService.showSuccess('Content Pushed Successfully')
    },error => {
      this.uiService.showError();
    })
  }


}
