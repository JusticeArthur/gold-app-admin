import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Pillar, topic } from 'src/app/services/auth.service';
import { RegionService } from 'src/app/services/dashboard/settings/region.service';
import {FormGroup, FormBuilder, FormControl, Validators, FormArray} from '@angular/forms';
import { TopicService } from 'src/app/services/dashboard/settings/topic.service';
import { UiService } from 'src/app/services/ui.service';
import { ContentService } from 'src/app/services/dashboard/content.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {
  pillars : Pillar[];
  topics : topic[];
  answer={};
  responses = [];
  questionTypes=[
    {name:'adherence'},
    {name: 'multichoice'},
    {name: 'number'},
    {name: 'text'},
  ];
  priorities = [
    {name:'Recommended'},
    {name: 'Urgent'},
    {name: 'Important'},
  ];
  fileUpload = true;
  dropZoneActive = false;
  file : File = null;
  steps : FormGroup;

  @ViewChild('fileInput',{static : false}) fileInput  : ElementRef<HTMLInputElement>;
  constructor(private fb: FormBuilder, private regionService:RegionService, private topicService:TopicService,private uiService:UiService,
    private contentService: ContentService,private router : Router) {}

  ngOnInit() {
    this.topicService.fetchPillars(84).subscribe(resp => {
      this.pillars = resp.data
    });

    this.steps = this.fb.group({
      'step1' : this.fb.group({
        'pillarId': new FormControl('',[Validators.required]),
        'topicId': new FormControl('',[Validators.required]),
        'question' :new FormControl('',[Validators.required]),
        'questionType': new FormControl('',[Validators.required]),
      }),
      'step2' : this.fb.array([
        this.stepTwoGroup
      ]),
      'step3': this.fb.group({
        'strongPoint': this.fb.control(''),
        'weakPoint': this.fb.control(''),
        'weight' :this.fb.control(''),
        'task' : this.fb.control(false)
      }),
      'step4' : this.fb.group({
        'task' : this.fb.control('',[Validators.required]),
        'priority' : this.fb.control('recommended',[Validators.required])
      })
    });
  }
/*component methods*/

  get stepOne(){
    return <FormGroup>this.steps.get('step1');
  }
  get stepTwo(){
    return <FormArray>this.steps.get('step2');
  }
  get stepThree(){
    return <FormGroup>this.steps.get('step3');
  }
  get stepFour(){
    return <FormGroup>this.steps.get('step4');
  }

  get stepTwoGroup(){
    return this.fb.group({
      'response' : this.fb.control('',[Validators.required]),
      'correct' : this.fb.control(false)
    });
  }

  get associatedTaskEnabled(){
    return (<FormGroup>this.steps.get('step3')).get('task').value;;
  }

  addStepTwoGroup(){
    this.stepTwo.push(this.stepTwoGroup);
  }
  removeStepTwoGroup(index){
    this.stepTwo.controls.splice(index,1);
  }
  selectionChange(event){
    this.topicService.fetchAllTopics(event).subscribe(resp => {
     this.topics = resp.data;
    });
  }
 addResponse(){
  this.responses.push(this.answer);
 }
 removeResponse(){
  this.responses.pop();
 }

 addQuestion(){
  // console.log(this.steps.value);
   let question : any = {};
   question.pillar_id = this.stepOne.get('pillarId').value;
   question.topic_id = this.stepOne.get('topicId').value;
   question.question = this.stepOne.get('question').value;
   question.question_type = this.stepOne.get('questionType').value;
   question.responses = this.stepTwo.value;
   question.strong_point = this.stepThree.get('strongPoint').value;
   question.weak_point = this.stepThree.get('weakPoint').value;
   question.weight = this.stepThree.get('weight').value;

   if(this.associatedTaskEnabled){
     question.task = this.stepFour.get('task').value;
     question.priority = this.stepFour.get('priority').value;
   }

   this.contentService.addQuestion(question).subscribe(resp => {
    this.uiService.showSuccess('community added successfully');
  },error => {
    this.uiService.showError();
  })

   console.log(question);

   //
 }

 dropZoneEvents(event : boolean){
    this.dropZoneActive = event;
 }

  onFileDropped(event){
    //check for the file extension before accepting
    this.file = event;
  }

  onUploadButtonClicked(){
    this.fileInput.nativeElement.click();
  }

  onUploadExcel(){
    let formData = new FormData();
    formData.append('upload',this.file);
    this.contentService.addBulk(formData).subscribe(resp => {
      console.log('success')
      this.router.navigate(['/dashboard/content']).then(then => {
        this.uiService.showSuccess('Upload successful');
      })
    },error => {
      this.uiService.showError();
    })
  }


}
