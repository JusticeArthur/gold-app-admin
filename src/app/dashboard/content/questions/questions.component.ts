import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ContentService} from "../../../services/dashboard/content.service";
import {Question} from "../../../models/question.model";
import {Paginatable} from "../../../services/env.service";
import {UiService} from "../../../services/ui.service";
import {DeletionService} from "../../../services/deletion.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, OnDestroy {

  questions : Question[] = [];
  deleteIndex;
  questionIndex;
  questionPage : Paginatable<Question>;
  allSubscriptions : Subscription[] = [];
  loadingMore = false;
  loadingQuestions = true;
  constructor(private deleteService : DeletionService,private uiService : UiService,private router : Router,private activatedRoute : ActivatedRoute,private contentService : ContentService) { }

  ngOnInit() {
    this.allSubscriptions.push(
      this.contentService.fetchFullQuestions().subscribe(page => {
        if(page){
          this.questionPage = page.data;
          this.questions = this.questionPage.data;
        }
        this.loadingQuestions = false;
      },error => {
        this.loadingQuestions = false;
      })
    );
    this.onDeleteTriggered();
  }

  goTo(link : string){
    this.router.navigate([`./${link}`],{relativeTo : this.activatedRoute})
  }

  loadMoreQuestions(){
    this.loadingMore = true;
    this.contentService.fetchFullQuestions(this.questionPage.next_page_url).subscribe(page => {
      this.loadingMore = false;
      if(page){
        this.questionPage = page.data;
        this.questions = [...this.questions,...this.questionPage.data]
      }
    },error => {
      this.loadingMore = false;
    })
  }
  triggerDeleteModal(content,row,index){
    this.deleteIndex = row.id;
    this.questionIndex = index;
    this.uiService.openModal(content);
  }

  onDeleteTriggered(){
    this.allSubscriptions.push(
      this.deleteService.status.subscribe((data : any) => {
        if(data){
          if(data.status === 'confirmed'){
            this.onDeleteQuestion();
          }
        }
      })
    )
  }

  onDeleteQuestion(){
    this.contentService.deleteQuestion(this.deleteIndex).subscribe(deletion => {
      this.questions.splice(this.questionIndex,1);
      this.uiService.showSuccess('deleted successfully');
    },error => {
      this.uiService.showError();
    })
  }

  ngOnDestroy(): void {
    this.allSubscriptions.forEach(subs => {
      subs.unsubscribe();
    })
  }

  onDeleteShow(type){

  }
}
