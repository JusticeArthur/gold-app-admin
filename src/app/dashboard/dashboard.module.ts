import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {DashboardComponent} from "./dashboard.component";
import {DashboardRoutingModule} from "./dashboard.routing.module";
import {HomeComponent} from "./home/home.component";
import {SidebarComponent} from "./partials/sidebar/sidebar.component";
import {HeaderComponent} from "./partials/header/header.component";
import {FooterComponent} from "./partials/footer/footer.component";
import {SettingsComponent} from "./settings/settings.component";
import { RegionComponent } from './settings/region/region.component';
import { DistrictComponent } from './settings/district/district.component';
import { ProgramComponent } from './settings/program/program.component';
import { PillarComponent } from './settings/pillar/pillar.component';
import { EducationComponent } from './settings/education/education.component';
import { TOPICComponent } from './settings/topic/topic.component';
import { UsersComponent } from './users/users.component';
import { AddComponent } from './users/add/add.component';
import { MinesComponent } from './settings/mines/mines.component';
import { CommunityComponent } from './settings/community/community.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { RoleComponent } from './settings/role/role.component';
import {ContentModule} from "./content/content.module";


@NgModule({
  declarations: [
    SidebarComponent,
    HeaderComponent,
    DashboardComponent,
    HomeComponent,
    FooterComponent,
    SettingsComponent,
    RegionComponent,
    DistrictComponent,
    ProgramComponent,
    PillarComponent,
    EducationComponent,
    TOPICComponent,
    UsersComponent,
    AddComponent,
    MinesComponent,
    CommunityComponent,
    RoleComponent,
  ],
  imports: [
    BsDatepickerModule,
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    ContentModule
  ]
})
export class DashboardModule { }
