import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {DashboardComponent} from "./dashboard.component";
import {SettingsComponent} from "./settings/settings.component";
import {AuthGuard} from "../guards/auth.guard";
import {RegionComponent} from "./settings/region/region.component";
import { DistrictComponent } from './settings/district/district.component';
import { ProgramComponent } from './settings/program/program.component';
import { PillarComponent } from './settings/pillar/pillar.component';
import { EducationComponent } from './settings/education/education.component';
import { TOPICComponent } from './settings/topic/topic.component';
import { UsersComponent } from './users/users.component';
import { MinesComponent } from './settings/mines/mines.component';
import { CommunityComponent } from './settings/community/community.component';
import { RoleComponent } from './settings/role/role.component';


const routes : Routes = [
  {path : '' , component : DashboardComponent, canActivate : [AuthGuard],canActivateChild : [AuthGuard] , children : [
      {path : '' , component : HomeComponent , pathMatch : 'full' },
      {path : 'settings' , component : SettingsComponent },
      {path : 'settings/regions' , component : RegionComponent, pathMatch : 'full'},
      {path : 'settings/districts', component : DistrictComponent},
      {path : 'settings/programs',component : ProgramComponent},
      {path : 'settings/pillars',component : PillarComponent},
      {path : 'settings/educational-level',component : EducationComponent},
      {path : 'settings/topics', component : TOPICComponent},
      {path : 'users', component : UsersComponent},
      {path : 'settings/mines', component : MinesComponent},
      {path : 'settings/communities', component : CommunityComponent},
      {path : 'settings/roles', component : RoleComponent},
      {path : 'content' , loadChildren : './content/content.module#ContentModule'},

    ]
  }
];
@NgModule({
  imports : [RouterModule.forChild(routes)],
  exports : [RouterModule]
})
export class DashboardRoutingModule {

}
