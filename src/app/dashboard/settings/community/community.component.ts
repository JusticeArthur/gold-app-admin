import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {DistrictService} from "../../../services/dashboard/settings/district.service";
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Region, Country, District, Community } from 'src/app/services/auth.service';
import { DeletionService } from 'src/app/services/deletion.service';
import { CommunitiesService } from 'src/app/services/dashboard/settings/communities.service';
import { UsersService } from 'src/app/services/dashboard/users.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityComponent implements OnInit {

  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedCommunity:any={};
  communityForm : FormGroup;
  regions: Region[];
  countries: Country[];
  districts: District[];
  editMode = false;
  pageData : Paginatable<Community> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Community' },
    { prop: 'district.name' , name: 'District' },
    { prop: 'district.region.name' , name : 'Region'},
    { prop: 'district.region.country.name' , name : 'Country'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService, private usersService:UsersService,
    private districtService:DistrictService, private deleteService:DeletionService, private communityService:CommunitiesService) { }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
   

    this.communityForm = this.fb.group({
      name : [null,[Validators.required]],
      regionId : [null, [Validators.required]],
      countryId : [null, [Validators.required]],
      districtId : [null,[Validators.required]],
    });
    
    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteCommunity();
        }
      }
    })
  }

  
  open(content) {
    this.editMode = false;
    this.communityForm.reset();
    this.uiService.openModal(content)
  }


  
  updatePageData(){
    this.communityService.fetchCommunity(this.perPage).subscribe(district =>{
      console.log(district);
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.districtService.fetchDistricts(this.perPage,this.searchQuery).subscribe(district => {
    //  this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.communityService.fetchPaginatedRecord(path).subscribe(community => {
      this.pageData = {...community.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.communityService.fetchCommunity(this.perPage).subscribe(community =>{
     this.pageData = {...community.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addCommunity(){
    let formData = new FormData();
    if (this.communityForm.valid) {
      formData.append('name',this.communityForm.get('name').value);
    formData.append('district_id',this.communityForm.get('districtId').value);
    this.communityService.addCommunity(formData).subscribe(resp => {
      if (!resp) {
        this.uiService.showSuccess('community added successfully');
        this.updatePageData();
      } else {
        this.uiService.showWarning('community already exist');
      }
      this.communityForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal(); 
    }
   
  }


  selectionChange(event){
    this.usersService.fetchDistrict(event).subscribe(resp => {
     this.districts = resp.data;
    });
  }

  selectionChanged(event){
    this.regionService.fetchRegionAll(event).subscribe(resp => {
     this.regions = resp.data;
    });
  }

  showEditModal(row,content){
   this.editMode = true;
   this.selectedCommunity = {...row};
    this.communityForm.get('name').setValue(row.name);
    this.communityForm.get('countryId').setValue(row.district.region.country_id);
    this.regionService.fetchRegionAll(this.selectedCommunity.district.region.country_id).subscribe(resp => {
      this.regions = resp.data;
     });
    this.communityForm.get('regionId').setValue(row.district.region_id);
    this.usersService.fetchDistrict(this.selectedCommunity.district.region_id).subscribe(resp => {
      this.districts = resp.data;
     });
    this.communityForm.get('districtId').setValue(row.district_id);
    
    
    
    this.uiService.openModal(content);
  }



  updateCommunity(){
    //this.processing = true;
    let formData  : any = {};
    formData.name = this.communityForm.get('name').value;
    formData.district_id = this.communityForm.get('districtId').value;
    this.communityService.updateCommunity(formData,this.selectedCommunity.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('community update successful');
      //this.processing = false;
      this.updatePageData();
      this.communityForm.reset();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedCommunity = row.id;
    this.uiService.openModal(content);
  }

  deleteCommunity(){
    //this.processing = true;
    this.communityService.deleteCommunity(this.selectedCommunity).subscribe(resp => {
     this.uiService.showSuccess('community deleted successfully');
     //this.processing = false;
     this.deleteService.setStatus({status : 'null'});
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }

  exportExcel(){
    this.communityService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'communities.xlsx')
    })
  }
}
