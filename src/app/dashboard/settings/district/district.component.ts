import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {DistrictService} from "../../../services/dashboard/settings/district.service";
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Region, Country, District } from 'src/app/services/auth.service';
import { DeletionService } from 'src/app/services/deletion.service';

@Component({
  selector: 'app-district',
  templateUrl: './district.component.html',
  styleUrls: ['./district.component.scss']
})
export class DistrictComponent implements OnInit {
  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedDistrict:any={};
  processing = false;
  districtForm : FormGroup;
  regions: Region[];
  countries: Country[];
  editMode = false;
  pageData : Paginatable<District> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'District' },
    { prop: 'region.name' , name : 'Region'},
    { prop: 'region.country.name' , name : 'Country'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService, 
    private districtService:DistrictService, private deleteService:DeletionService) { }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
   

    this.districtForm = this.fb.group({
      name : [null,[Validators.required]],
      regionId : [null, [Validators.required]],
      countryId : [null, [Validators.required]],
    });
    
    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteDistrict();
        }
      }
    })
  }

  
  open(content) {
    this.districtForm.reset();
    this.uiService.openModal(content)
  }


  
  updatePageData(){
    this.districtService.fetchDistricts(this.perPage).subscribe(district =>{
      console.log(district);
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.districtService.fetchDistricts(this.perPage,this.searchQuery).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.districtService.fetchPaginatedRecord(path).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.districtService.fetchDistricts(this.perPage).subscribe(district =>{
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addDistrict(){
    let formData = new FormData();
    formData.append('name',this.districtForm.get('name').value);
    formData.append('region_id',this.districtForm.get('regionId').value);
    this.districtService.addDistrict(formData).subscribe(resp => {
      if (!resp) {
        this.uiService.showSuccess('district added successfully');
        this.updatePageData();
      } else {
        this.uiService.showWarning('district already exist');
      }
      this.districtForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal();
  }


  

  selectionChanged(event){
    this.regionService.fetchRegionAll(event).subscribe(resp => {
     this.regions = resp.data;
    });
  }

  showEditModal(row,content){
   this.editMode = true;
    this.districtForm.get('name').setValue(row.name);
    this.selectedDistrict = {...row};
    this.regionService.fetchRegionAll(row.region.country_id).subscribe(resp => {
      this.regions = resp.data;
      this.districtForm.get('regionId').setValue(row.region_id);
     });
    
    this.districtForm.get('countryId').setValue(row.region.country_id);
    
    this.uiService.openModal(content);
  }



  updateDistrict(){
    this.processing = true;
    let formData  : any = {};
    formData.name = this.districtForm.get('name').value;
    formData.region_id = this.districtForm.get('regionId').value;
    this.districtService.updateDistrict(formData,this.selectedDistrict.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      this.processing = false;
      this.updatePageData();
      this.districtForm.reset();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedDistrict = row.id;
    this.uiService.openModal(content);
  }

  deleteDistrict(){
    //this.processing = true;
    this.districtService.deleteDistrict(this.selectedDistrict).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     //this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  exportExcel(){
    this.districtService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'districts.xlsx')
    })
  }
}
