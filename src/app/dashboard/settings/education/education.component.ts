import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeletionService } from 'src/app/services/deletion.service';
import { PillarService } from 'src/app/services/dashboard/settings/pillar.service';
import { EducationService } from 'src/app/services/dashboard/settings/education.service';
import { DistrictService } from 'src/app/services/dashboard/settings/district.service';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  
  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectededucation:any={};
  processing = false;
  educationForm : FormGroup;
  editMode = false;
  pageData : Paginatable<any> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Education Level' },
    { prop: 'description' , name : 'Description'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  constructor(private fb:FormBuilder,private uiService:UiService,private pillarService:PillarService, private deleteService:DeletionService,
    private educationService:EducationService, private districtService:DistrictService) { }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
   

    this.educationForm = this.fb.group({
      name : [null,[Validators.required]],
      description : [null],
    });
    
    this.updatePageData();

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteEducation();
        }
      }
    })
  }

  
  open(content) {
    this.editMode = false;
    this.educationForm.reset();
    this.uiService.openModal(content)
  }


 
  updatePageData(){
    this.educationService.fetchEducation(this.perPage).subscribe(resp =>{
      console.log(resp);
      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.educationService.fetchEducation(this.perPage,this.searchQuery).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.educationService.fetchPaginatedRecord(path).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.educationService.fetchEducation(this.perPage).subscribe(district =>{
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addEducation(){
    this.editMode = false;
    let formData = new FormData();
    formData.append('name',this.educationForm.get('name').value);
    formData.append('description',this.educationForm.get('description').value);
    this.educationService.addEducation(formData).subscribe(resp => {
      if (!resp) {
          this.uiService.showSuccess('Educational level added successfully');
        this.updatePageData();
      } else {
        this.uiService.showWarning('Educational level already exist');
      }
    
      this.educationForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal();
  }


  

  showEditModal(row,content){
   this.editMode = true;
    this.educationForm.get('name').setValue(row.name);
    this.selectededucation = {...row};  
    this.educationForm.get('description').setValue(row.description);
    
    this.uiService.openModal(content);
  }



  updateEducation(){
    //this.processing = true;
    let formData  : any = {};
    formData.name = this.educationForm.get('name').value;
    formData.description = this.educationForm.get('description').value;
    this.educationService.updateEducation(formData,this.selectededucation.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      this.processing = false;
     this.updatePageData();
      this.educationForm.reset();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectededucation = row.id;
    this.uiService.openModal(content);
  }

  deleteEducation(){
    this.processing = true;
    this.educationService.deleteEducation(this.selectededucation).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }

  exportExcel(){
    this.educationService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'edu_levels.xlsx')
    })
  }


}
