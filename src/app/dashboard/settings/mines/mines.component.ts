import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {DistrictService} from "../../../services/dashboard/settings/district.service";
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Region, Country, District, UserDetails, Community } from 'src/app/services/auth.service';
import { DeletionService } from 'src/app/services/deletion.service';
import { ProgramService } from 'src/app/services/dashboard/settings/program.service';
import { UsersService } from 'src/app/services/dashboard/users.service';
import { MinesService } from 'src/app/services/dashboard/settings/mines.service';

@Component({
  selector: 'app-mines',
  templateUrl: './mines.component.html',
  styleUrls: ['./mines.component.scss']
})
export class MinesComponent implements OnInit {

  bsInlineValue = new Date();
  bsInlineRangeValue: Date[];
  maxDate = new Date();

  
  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedMine:any={};
  mineForm : FormGroup;
  main=true;
  regions: Region[];
  community: Community[];
  districts: District[];
  users: any[];
  ownership: Array<any>=[
    {value:'Sole_proprietorship',own_name:'Sole proprietorship'},
    {value:'company',own_name:'company'},
    {value:'cooperative',own_name:'cooperative'},
    {value:'community_minning',own_name:'community minning'}
  ];
  countries: Country[];
  editMode = false;
  pageData : Paginatable<UserDetails> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Mine' },
    { prop: 'community.district.region.country.name' , name : 'Location'},
    { prop: 'community.district.region.name' , name : 'Location'},
    { prop: 'community.district.name' , name : 'Location'},
    { prop: 'community.name' , name : 'Location'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;

  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService,private programService:ProgramService,
    private districtService:DistrictService, private deleteService:DeletionService, private usersService:UsersService,private minesService:MinesService) {
      
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsInlineRangeValue = [this.bsInlineValue, this.maxDate];
     }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
   

    this.mineForm = this.fb.group({
      name : [null,[Validators.required]],
      districtId : [null, [Validators.required]],
      regionId : [null, [Validators.required]],
      countryId : [null, [Validators.required]],
      communityId : [null, [Validators.required]],
      address : [null, [Validators.required]],
    });
    
    console.log(this.ownership);
    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.programService.fetchUsers().subscribe(resp => {
      this.users = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteMine();
        }
      }
    })
  }



  
  open(content) {
    this.mineForm.reset();
   this.uiService.openModal(content)
   
  }

  close(content) {
    this.mineForm.reset();
    this.uiService.openModal(content)
    
  }

  
  updatePageData(){
    this.minesService.fetchMines(this.perPage).subscribe(resp =>{
      
      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.minesService.fetchMines(this.perPage,this.searchQuery).subscribe(resp => {
      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.minesService.fetchPaginatedRecord(path).subscribe(resp => {
      this.pageData = {...resp.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.minesService.fetchMines(this.perPage).subscribe(district =>{
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addMine(){
    let formData = new FormData();
    formData.append('name',this.mineForm.get('name').value);
    formData.append('community_id',this.mineForm.get('communityId').value);
    formData.append('address',this.mineForm.get('address').value);
    this.minesService.addMine(formData).subscribe(resp => {
      this.uiService.showSuccess('mine added successfully');
      
      this.updatePageData();
      this.mineForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal();
  }


  

  selectionChange(event){
    this.usersService.fetchDistrict(event).subscribe(resp => {
     this.districts = resp.data;
     
    });
  }

  selectionChanged(event){
    this.regionService.fetchRegionAll(event).subscribe(resp => {
     this.regions = resp.data;

    });
  }

  
  selectionChang(event){
    this.minesService.fetchCommunity(event).subscribe(resp => {
     this.community = resp.data;

    });
  }

  showEditModal(row,content){
 this.editMode = true;
   this.mineForm.get('name').setValue(row.name);
   this.mineForm.get('countryId').setValue(row.community.district.region.country_id);
   this.regionService.fetchRegionAll(row.community.district.region.country_id).subscribe(resp => {
    this.regions = resp.data;
   });
   this.mineForm.get('regionId').setValue(row.community.district.region_id);
   this.usersService.fetchDistrict(row.community.district.region_id).subscribe(resp => {
    this.districts = resp.data;
    
   });
   this.mineForm.get('districtId').setValue(row.community.district_id);
  
    this.minesService.fetchCommunity(row.community.district_id).subscribe(resp => {
     this.community = resp.data;
    });
   this.mineForm.get('communityId').setValue(row.community_id);
   this.mineForm.get('address').setValue(row.address);
    this.selectedMine = {...row};
  
    
    //this.mineForm.get('countryId').setValue(row.region.country_id);
    
    this.uiService.openModal(content);
   //this.open(content);
  }



  updateMine(){
    //this.processing = true;
    let formData  : any = {};
    formData.name=this.mineForm.get('name').value;
    formData.address=this.mineForm.get('address').value;
    formData.community_id=this.mineForm.get('communityId').value;
    this.minesService.updateMine(formData,this.selectedMine.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      //this.processing = false;
      this.updatePageData();
      this.mineForm.reset();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedMine = row.id;
    this.uiService.openModal(content);
  }

  deleteMine(){
    //this.processing = true;
    console.log(this.selectedMine)
    this.minesService.deleteMine(this.selectedMine).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     //this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }

  exportExcel(){
    this.minesService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'mines.xlsx')
    })
  }
}
