
import { Component, OnInit, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeletionService } from 'src/app/services/deletion.service';
import { PillarService } from 'src/app/services/dashboard/settings/pillar.service';
import { Subscription } from 'rxjs';
import { Country } from 'src/app/services/auth.service';

@Component({
  selector: 'app-pillar',
  templateUrl: './pillar.component.html',
  styleUrls: ['./pillar.component.scss']
})
export class PillarComponent implements OnInit,OnDestroy {

  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedPillar:any={};
  pillarForm : FormGroup;
  editMode = false;
  processing = false;
  countries: any;
  pageData : Paginatable<any> = null;
  perPage = 10;
  searchQuery = '';
  standards: Array<any>=[
    {value:'land',name:'land'},
    {value:'people',name:'people'},
    {value:'crop',name:'crop'},
    {value:'business',name:'business'}
  ];
  columns : Array<any> =[
    { prop: 'name' , name: 'Pillar' },
    { prop: 'standard' , name : 'standard'},
    { prop: 'country.name' , name : 'Country'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService, 
    private pillarService:PillarService, private deleteService:DeletionService) { }
    allSubscriptions : Subscription[] = [];
  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });

   

    this.pillarForm = this.fb.group({
      name : [null,[Validators.required]],
      standard : [null,[Validators.required]],
      countryId : [null,[Validators.required]],
    });
    
    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.allSubscriptions.push(
      this.deleteService.status.subscribe((data : any) => {
        if(data){
          if(data.status === 'confirmed'){
            this.deletePillar();
          }
        }
      })
    )
    
  }

  ngOnDestroy(){
    this.allSubscriptions.forEach(value=>{
      value.unsubscribe();
    })
  }


  
  open(content) {
    this.editMode = false;
    this.pillarForm.reset();
    this.uiService.openModal(content)
  }


 
  updatePageData(){
    this.pillarService.fetchPillars(this.perPage).subscribe(resp =>{
      
      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.pillarService.fetchPillars(this.perPage,this.searchQuery).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.pillarService.fetchPaginatedRecord(path).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.pillarService.fetchPillars(this.perPage).subscribe(district =>{
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addPillar(){
    this.editMode = false;
    let formData = new FormData();
    formData.append('name',this.pillarForm.get('name').value);
    formData.append('standard',this.pillarForm.get('standard').value);
    formData.append('country_id',this.pillarForm.get('countryId').value)
    this.pillarService.addPillars(formData).subscribe(resp => {
      if (!resp) {
       this.uiService.showSuccess('pillar added successfully');
       this.updatePageData();
      }else{
        this.uiService.showWarning('Standard exist for country');
      }
      
     
      this.pillarForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal();
  }


  

  showEditModal(row,content){
   this.editMode = true;
   this.selectedPillar = {...row};
    this.pillarForm.get('name').setValue(row.name);
    this.pillarForm.get('standard').setValue(row.standard); 
    this.pillarForm.get('countryId').setValue(row.country_id);
    
    this.uiService.openModal(content);
  }



  updatePillar(){
    this.processing = true;
    let formData  : any = {};
    formData.name = this.pillarForm.get('name').value;
    formData.standard = this.pillarForm.get('standard').value;
    formData.country_id = this.pillarForm.get('countryId').value;
    this.pillarService.updatePillar(formData,this.selectedPillar.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      this.processing = false;
     this.updatePageData();
      this.pillarForm.reset();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedPillar = row.id;
    this.uiService.openModal(content);
  }

  deletePillar(){
    this.processing = true;
    console.log(this.selectedPillar)
    this.pillarService.deletePillar(this.selectedPillar).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }


  exportExcel(){
    this.pillarService.exportExcel().subscribe((resp : any) => {
      this.regionService.exportAsExcelFile(resp,'pillars.xlsx')
    })
  }
}
