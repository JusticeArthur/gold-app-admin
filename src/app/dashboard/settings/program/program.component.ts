import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeletionService } from 'src/app/services/deletion.service';
import { ProgramService } from 'src/app/services/dashboard/settings/program.service';
import { Program } from 'src/app/services/auth.service';
import { DistrictService } from 'src/app/services/dashboard/settings/district.service';

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {

  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedProgram:any={};
  programForm : FormGroup;
  processing = false;
  editMode = false;
  pageData : Paginatable<Program> = null;
  users:any={};
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Program' },
    { prop: 'users.first_name', name: 'Manager'},
    { prop: 'code', name: 'Code'},
    { prop: 'description' , name : 'Description'},

  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;

  constructor(private fb:FormBuilder,private uiService:UiService, private deleteService:DeletionService,private districtService:DistrictService, private programService:ProgramService) { }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
  

    this.programForm = this.fb.group({
      name : [null,[Validators.required]],
      managerId : [null],
      code:[null],
      description : [null, [Validators.required]],
    });
    
    this.updatePageData();
    this.programService.fetchUsers().subscribe(resp => {
      this.users = resp.data;
    });
    

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteProgram();
        }
      }
    })
  }

  
  open(content) {
    this.editMode = false;
    this.programForm.reset();
    this.uiService.openModal(content)
  }


  
  updatePageData(){
  this.programService.fetchProgram(this.perPage).subscribe(program =>{
    this.pageData = {...program.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.programService.fetchProgram(this.perPage,this.searchQuery).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.programService.fetchPaginatedRecord(path).subscribe(program => {
      this.pageData = {...program.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.programService.fetchProgram(this.perPage).subscribe(program =>{
      this.pageData = {...program.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addProgram(){
    let formData = new FormData();
    formData.append('name',this.programForm.get('name').value);
    formData.append('manager_id',this.programForm.get('managerId').value);
    formData.append('code',this.programForm.get('code').value);
    formData.append('description',this.programForm.get('description').value)
    this.programService.addProgram(formData).subscribe(resp => {
      if (!resp) {
        this.uiService.showSuccess('program added successfully');
        this.updatePageData();
      }else{
        this.uiService.showWarning('Program already exist');
      }
      this.programForm.reset();
    },error => {
      this.uiService.showError();
    })
    this.uiService.closeModal();
  }


 
  showEditModal(row,content){
   this.editMode = true;
   this.selectedProgram = {...row};
    this.programForm.get('name').setValue(row.name);
    this.programForm.get('code').setValue(row.code);
    this.programForm.get('managerId').setValue(row.manager_id);
    this.programForm.get('description').setValue(row.description);
    
    this.uiService.openModal(content);
  }



  updateProgram(){
    this.processing = true;
    let formData  : any = {};
    formData.name = this.programForm.get('name').value;
    formData.manager_id = this.programForm.get('managerId').value;
    formData.description = this.programForm.get('description').value;
    formData.code = this.programForm.get('code').value;
    this.programService.updateProgram(formData,this.selectedProgram.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      this.processing = false;
      this.updatePageData();
      this.programForm.reset();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedProgram = row.id;
    this.uiService.openModal(content);
  }

  deleteProgram(){
    this.processing = true;
    this.programService.deleteProgram(this.selectedProgram).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }

  exportExcel(){
    this.programService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'program.xlsx')
    })
  }
}
