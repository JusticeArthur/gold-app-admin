import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {Country, Region} from "../../../services/auth.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import {UiService} from "../../../services/ui.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DeletionService} from "../../../services/deletion.service";

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
})
export class RegionComponent implements OnInit {

  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  processing = false;
  regionForm : FormGroup;
  pageData : Paginatable<Region> = null;
  perPage = 10;
  searchQuery = '';
  editMode = false;
  columns : Array<any> = [
    { prop: 'name' , name: 'Name' },
    { prop: 'country.name' , name : 'Country Name'},
    { prop: 'country.code' , name : 'Country Code'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  countries : Country[];
  selectedRegion : any =  {};
  deleteIndex = 0;

  constructor(private regionService : RegionService,private deleteService : DeletionService,
              private uiService : UiService,private fb : FormBuilder) {
  }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
    //pusn to the columns
    //regions form
    this.regionForm = this.fb.group({
      name : [null,[Validators.required]],
      countryId : [null, [Validators.required]],
    });
    this.updatePageData();

    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteRegion();
        }
      }
    })
  }

  updatePageData(){
    this.loadingIndicator = true;
    this.regionService.fetchRegions(this.perPage).subscribe(regions =>{
      this.pageData = {...regions.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.regionService.fetchRegions(this.perPage,this.searchQuery).subscribe(regions => {
      this.pageData = {...regions.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }

  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.regionService.fetchPaginatedRecord(path).subscribe(resp => {
      this.pageData = {...resp.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }

  pageChanged(){
    this.loadingIndicator = true;
    this.regionService.fetchRegions(this.perPage).subscribe(regions =>{
      this.pageData = {...regions.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }

  open(content) {
    this.editMode = false;
    this.selectedRegion = {};
    this.regionForm.reset();
    this.uiService.openModal(content)
  }

  addRegion(){
    this.processing = true;
    let formData = new FormData();
    formData.append('name',this.regionForm.get('name').value);
    formData.append('country_id',this.regionForm.get('countryId').value);
    this.regionService.addRegion(formData).subscribe(resp => {
      this.uiService.showSuccess('region added successfully');
      this.uiService.closeModal();
      this.processing = false;
      this.regionForm.reset();
      this.updatePageData();
    },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }

  showEditModal(row,content){
    this.editMode = true;
    this.regionForm.get('name').setValue(row.name);
    this.selectedRegion = {...row};
    this.regionForm.get('countryId').setValue(row.country_id);
    this.uiService.openModal(content);
  }

  updateRegion(){
    this.processing = true;
    let formData  : any = {};
    formData.name = this.regionForm.get('name').value;
    formData.country_id = this.regionForm.get('countryId').value;

    this.regionService.updateRegion(formData,this.selectedRegion.id).subscribe(resp => {
      this.uiService.closeModal();
      this.uiService.showSuccess('update successful');
      this.processing = false;
      this.updatePageData();
      this.regionForm.reset();
    },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }

  triggerDeleteModal(content,row){
    this.deleteIndex = row.id;
    this.uiService.openModal(content);
  }

  deleteRegion(){
    this.processing = true;
    this.regionService.deleteRegion(this.deleteIndex).subscribe(resp => {
      this.uiService.showSuccess('deleted successfully');
      this.deleteService.setStatus({status : 'null'});
      this.processing = false;
      this.updatePageData();
    },error => {
      this.uiService.showError();
      this.processing = false;
    })
  }

  exportExcel(){
    this.regionService.exportExcel().subscribe((resp : any) => {
      this.regionService.exportAsExcelFile(resp,'regions.xlsx')
    })
  }
}
