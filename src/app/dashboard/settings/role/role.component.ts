import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { DeletionService } from 'src/app/services/deletion.service';
import { ProgramService } from 'src/app/services/dashboard/settings/program.service';
import { Program } from 'src/app/services/auth.service';
import {  } from '@angular/forms'; import {  } from '@angular/forms';
import { RolesService } from 'src/app/services/dashboard/settings/roles.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {


  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedRole:any={};
  roleForm : FormGroup;
  
  editMode = false;
  pageData : Paginatable<Program> = null;
  users:any={};
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Program' },

  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;

  constructor(private fb:FormBuilder,private uiService:UiService, private rolesService:RolesService, private deleteService:DeletionService, private programService:ProgramService) { }
  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
  

    this.roleForm = this.fb.group({
      name : [null,[Validators.required]],
      managerId : [null],
      code:[null],
      permission : [],
      description : [null, [Validators.required]],
     // permissionss : FormArray([])
    });
    
    this.updatePageData();
    this.rolesService.fetchPermissions().subscribe(resp => {
      this.users = resp.data;
    });

    
    

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteRole();
        }
      }
    })
  }

  
  open(content) {
    this.editMode = false;
    this.roleForm.reset();
    this.uiService.openModal(content)
  }

 
  
  updatePageData(){
  this.rolesService.fetchRole(this.perPage).subscribe(program =>{
    console.log(program);
    this.pageData = {...program.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.rolesService.fetchRole(this.perPage,this.searchQuery).subscribe(district => {
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.rolesService.fetchPaginatedRecord(path).subscribe(program => {
      this.pageData = {...program.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.rolesService.fetchRole(this.perPage).subscribe(program =>{
      this.pageData = {...program.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }
 

  addRole(){
    let formData = new FormData();
    formData.append('name',this.roleForm.get('name').value);
    formData.append('description',this.roleForm.get('description').value);
    formData.append('permission',this.roleForm.get('permission').value)
    this.rolesService.addRole(formData).subscribe(resp => {
      this.uiService.showSuccess('program added successfully');
      this.updatePageData();
     this.roleForm.reset();
   },error => {
     this.uiService.showError();
    })
   this.uiService.closeModal();
  }


 
  showEditModal(row,content){
   this.editMode = true;
   this.selectedRole = {...row};
    this.roleForm.get('name').setValue(row.name);
    this.roleForm.get('permission').setValue(row.pemissions);
    
    this.uiService.openModal(content);
  }



  updateRole(){
    //this.processing = true;
    let formData  : any = {};
    formData.name = this.roleForm.get('name').value;
    this.rolesService.updateRole(formData,this.selectedRole.id).subscribe(resp => {
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      //this.processing = false;
      this.updatePageData();
      this.roleForm.reset();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedRole = row.id;
    this.uiService.openModal(content);
  }

  deleteRole(){
    //this.processing = true;
    console.log(this.selectedRole)
    this.rolesService.deleteRole(this.selectedRole).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     //this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


}
