import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { RegionService } from 'src/app/services/dashboard/settings/region.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  count:any = null;
  

  constructor(private router : Router,private activatedRoute : ActivatedRoute, private regionService:RegionService) { }

  ngOnInit() {

    this.regionService.fetchCount().subscribe(resp => {
      this.count= resp;
    });
  }

  goTo(link : string){
    this.router.navigate([`./${link}`],{relativeTo : this.activatedRoute})
  }



}
