import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TOPICComponent } from './topic.component';

describe('TOPICComponent', () => {
  let component: TOPICComponent;
  let fixture: ComponentFixture<TOPICComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TOPICComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TOPICComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
