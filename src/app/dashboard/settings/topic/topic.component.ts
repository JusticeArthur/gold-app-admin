import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {RegionService} from "../../../services/dashboard/settings/region.service";
import {DistrictService} from "../../../services/dashboard/settings/district.service";
import {UiService} from "../../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Region, Country, District, topic } from 'src/app/services/auth.service';
import { DeletionService } from 'src/app/services/deletion.service';
import { TopicService } from 'src/app/services/dashboard/settings/topic.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TOPICComponent implements OnInit {
  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedTopic:any={};
  topicForm : FormGroup;
  file : File;
  pillars : any;
  countries : any;
  editMode = false;
  pageData : Paginatable<topic> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'name' , name: 'Topic' },
    { prop: 'pillars.name' , name : 'Pillar'},
    { prop: 'description' , name : 'description'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;
  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService, 
    private districtService:DistrictService, private deleteService:DeletionService, private topicService:TopicService) { }

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });
   

    this.topicForm = this.fb.group({
      name : [null,[Validators.required]],
      countryId : [null,[Validators.required]],
      pillarId : [null, [Validators.required]],
      pic : [null,[Validators.required]],
      description : [null],
    });
    
    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteTopic();
        }
      }
    })
  }

  
  open(content) {
    this.editMode = false;
    this.topicForm.reset();
    this.uiService.openModal(content)
  }


  
  updatePageData(){
    this.topicService.fetchTopics(this.perPage).subscribe(topic =>{
      this.pageData = {...topic.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.topicService.fetchTopics(this.perPage,this.searchQuery).subscribe(topic => {
      this.pageData = {...topic.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.topicService.fetchPaginatedRecord(path).subscribe(topic => {
      this.pageData = {...topic.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }

  selectionChanged(event){
    this.topicService.fetchPillars(event).subscribe(resp => {
     this.pillars = resp.data;
    });
  }
 

  pageChanged(){
    this.loadingIndicator = true;
    this.topicService.fetchTopics(this.perPage).subscribe(topic =>{
      this.pageData = {...topic.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }


  onImageChange(event){
    this.file = event.target.files[0];
  }
 

  addTopic(){
    
    let formData = new FormData();
    formData.append('name',this.topicForm.get('name').value);
    formData.append('pillar_id',this.topicForm.get('pillarId').value);
    formData.append('pic',this.file);
    formData.append('description',this.topicForm.get('description').value);
    this.topicService.addTopic(formData).subscribe(resp => {
      if (!resp) {
        this.uiService.showSuccess('topic added successfully');
        this.updatePageData();
       }else{
         this.uiService.showWarning('topic exist for this pillar');
       }
     
      this.updatePageData();
      this.topicForm.reset();
    },error => {
      this.uiService.showError();
   })
    this.uiService.closeModal();
  }


 

  showEditModal(row,content){
   this.editMode = true;
    this.topicForm.get('name').setValue(row.name);
    this.selectedTopic = {...row};
    this.topicForm.get('countryId').setValue(row.pillars.country_id);
    this.topicService.fetchPillars(row.pillars.country_id).subscribe(resp => {
      this.pillars = resp.data;
     });
      this.topicForm.get('pillarId').setValue(row.pillar_id);
   
    this.topicForm.get('description').setValue(row.description);
    
    this.uiService.openModal(content);
  }



  updateTopic(){
    //this.processing = true;
    let formData  : any = {};
    formData.name = this.topicForm.get('name').value;
    formData.pillar_id = this.topicForm.get('pillarId').value;
   // formData.pic = this.file;
   // console.log(formData.pic)
    formData.description = this.topicForm.get('description').value;
    this.topicService.updateTopic(formData,this.selectedTopic.id).subscribe(resp => {
   //   console.log(resp);
     this.uiService.closeModal();
     this.uiService.showSuccess('update successful');
      //this.processing = false;
      this.updatePageData();
      this.topicForm.reset();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedTopic = row.id;
    this.uiService.openModal(content);
  }

  deleteTopic(){
    //this.processing = true;
    this.topicService.deleteTopic(this.selectedTopic).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     //this.processing = false;
     this.deleteService.setStatus({status : 'null'});
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }

  exportExcel(){
    this.topicService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'topics.xlsx')
    })
  }

}
