import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {RegionService} from "../../services/dashboard/settings/region.service";
import {DistrictService} from "../../services/dashboard/settings/district.service";
import {UiService} from "../../services/ui.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Paginatable} from "../../services/env.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Region, Country, District, UserDetails, Community } from 'src/app/services/auth.service';
import { DeletionService } from 'src/app/services/deletion.service';
import { UsersService } from 'src/app/services/dashboard/users.service';
import { MinesService } from 'src/app/services/dashboard/settings/mines.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('actionField',{static : true}) tableActionField : TemplateRef<any>;
  @ViewChild('actionHeader',{static : true}) tableActionHeader : TemplateRef<any>;
  selectedDistrict:any={};


  userForm : FormGroup;
  main=true;
  regions: Region[];
  community: Community[];
  districts: District[];
  countries: Country[];
  editMode = false;
  pageData : Paginatable<UserDetails> = null;
  perPage = 10;
  searchQuery = '';
  columns : Array<any> =[
    { prop: 'first_name' , name: 'User' },
    { prop: 'email' , name : 'E-mail'},
    { prop: 'status', name : 'status'},
  ];
  columnMode = ColumnMode;
  reorderable = true;

  loadingIndicator = true;

  minDate: Date;
  maxDate: Date;



  constructor(private regionService:RegionService,private fb:FormBuilder,private uiService:UiService, private minesService:MinesService,
    private districtService:DistrictService, private deleteService:DeletionService, private usersService:UsersService) {

this.minDate = new Date();
    this.maxDate = new Date();
     this.maxDate.setFullYear(this.minDate.getFullYear() - 18);
     this.minDate.setFullYear(this.maxDate.getFullYear() - 98)
    this.maxDate.setDate(this.maxDate.getDate() + 7);


      this.userForm =this.fb.group ({
      first_name : [null,[Validators.required]],
      last_name : [null,[Validators.required]],
      other_name : [null,[Validators.required]],
      username : [null,[Validators.required]],
      districtId : [null],
      regionId : [null],
      dob : [null],
      countryId : [null],
      gender : [null],
      phone : [null],
      email : [null,[Validators.email]],
      communityId : [null],
      address : [null],
    });}

  ngOnInit() {
    this.columns.push({
      cellTemplate: this.tableActionField,
      headerTemplate: this.tableActionHeader,
      name: 'Actions'
    });




    this.updatePageData();
    this.regionService.fetchCountries().subscribe(resp => {
      this.countries = resp.data;
    });

    this.deleteService.status.subscribe((data : any) => {
      if(data){
        if(data.status === 'confirmed'){
          this.deleteDistrict();
        }
      }
    })
  }




  open(content) {
    this.editMode = false;
    this.userForm.reset();
    //this.uiService.openModal(content)
    this.main = false;
  }

  close(content) {
    //this.districtForm.reset();
    //this.uiService.openModal(content)
    this.main = true;
  }


  updatePageData(){
    this.usersService.fetchUsers(this.perPage).subscribe(resp =>{

      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    });
  }

  queryChanged(){
    this.loadingIndicator = true;
    this.usersService.fetchUsers(this.perPage,this.searchQuery).subscribe(resp => {
      this.pageData = {...resp.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false;
    })
  }


  fetchRecord(args){
    let pageNumber = args.offset + 1;
    let path = `${this.pageData.path}?page=${pageNumber}&size=${this.perPage}&search=${this.searchQuery}`;
    this.loadingIndicator = true;
    this.usersService.fetchPaginatedRecord(path).subscribe(resp => {
      this.pageData = {...resp.data};
      this.loadingIndicator = false;
    },error => {
      this.loadingIndicator = false;
    })
  }


  pageChanged(){
    this.loadingIndicator = true;
    this.usersService.fetchUsers(this.perPage).subscribe(district =>{
      this.pageData = {...district.data};
      this.loadingIndicator = false
    },error => {
      this.loadingIndicator = false
    })
  }


  addUser(){
    let incoming_date = new Date(this.userForm.get('dob').value).toLocaleDateString();
    let formData = new FormData();
    formData.append('first_name',this.userForm.get('first_name').value);
    formData.append('last_name',this.userForm.get('last_name').value);
    formData.append('other_name',this.userForm.get('other_name').value);
    formData.append('username',this.userForm.get('username').value);
    formData.append('email',this.userForm.get('email').value);
    formData.append('phone',this.userForm.get('phone').value);
    formData.append('dob',incoming_date);
    formData.append('gender',this.userForm.get('gender').value);
    formData.append('district_id',this.userForm.get('districtId').value);
    formData.append('country_id',this.userForm.get('countryId').value);
    formData.append('region_id',this.userForm.get('regionId').value);
    formData.append('community_id',this.userForm.get('communityId').value);
    formData.append('address',this.userForm.get('address').value);
  this.usersService.addUser(formData).subscribe(resp => {
   this.uiService.showSuccess('User added successfully');
    this.updatePageData();
      this.userForm.reset();
  },error => {
    this.uiService.showError();
  })
    this.uiService.closeModal();
   }




  selectionChange(event){
    this.usersService.fetchDistrict(event).subscribe(resp => {
     this.districts = resp.data;

    });
  }

  selectionChanged(event){
    this.regionService.fetchRegionAll(event).subscribe(resp => {
     this.regions = resp.data;

    });
  }

  selectionChang(event){
    this.minesService.fetchCommunity(event).subscribe(resp => {
     this.community = resp.data;

    });
  }

  showEditModal(row){
   this.editMode = true;
   this.userForm.get('first_name').setValue(row.first_name);
   this.userForm.get('last_name').setValue(row.last_name);
   this.userForm.get('other_name').setValue(row.other_name);
   this.userForm.get('username').setValue(row.username);
   this.userForm.get('email').setValue(row.email);
   this.userForm.get('phone').setValue(row.phone);
 //  this.userForm.get('dob').setdate('');
   this.userForm.get('gender').setValue(row.gender);

    this.selectedDistrict = {...row};


    this.userForm.get('countryId').setValue(row.community.district.region.country_id);
    this.regionService.fetchRegionAll(row.community.district.region.country_id).subscribe(resp => {
     this.regions = resp.data;
    });
    this.userForm.get('regionId').setValue(row.community.district.region_id);
   this.usersService.fetchDistrict(row.community.district.region_id).subscribe(resp => {
     this.districts = resp.data;
  });
    this.userForm.get('districtId').setValue(row.community.district_id);

    this.minesService.fetchCommunity(row.community.district_id).subscribe(resp => {
      this.community = resp.data;

     });
    this.userForm.get('communityId').setValue(row.community_id);
    this.userForm.get('address').setValue(row.address);
   // this.uiService.openModal(content);
   //this.open(content);
   this.main = false;
  }



  updateUser(){
    //this.processing = true;
    let incoming_date = new Date(this.userForm.get('dob').value).toLocaleDateString();
    let formData  : any = {};
    formData.first_name=this.userForm.get('first_name').value;
    formData.last_name=this.userForm.get('last_name').value;
    formData.other_name=this.userForm.get('other_name').value;
    formData.username=this.userForm.get('username').value;
    formData.email=this.userForm.get('email').value;
    formData.dob = incoming_date;
    formData.phone=this.userForm.get('phone').value;
    formData.gender=this.userForm.get('gender').value;
    formData.district_id=this.userForm.get('districtId').value;
    formData.country_id=this.userForm.get('countryId').value;
    formData.region_id=this.userForm.get('regionId').value;
    formData.community_id=this.userForm.get('communityId').value;
    this.usersService.updateUser(formData,this.selectedDistrict.id).subscribe(resp => {
     this.main = true;
     this.uiService.showSuccess('update successful');
      //this.processing = false;
      this.updatePageData();
      this.userForm.reset();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  triggerDeleteModal(content,row){
   this.selectedDistrict = row.id;
    this.uiService.openModal(content);
  }

  deleteDistrict(){
    //this.processing = true;
    console.log(this.selectedDistrict)
    this.usersService.deleteUser(this.selectedDistrict).subscribe(resp => {
     this.uiService.showSuccess('deleted successfully');
     this.deleteService.setStatus({status : 'null'});
     //this.processing = false;
      this.updatePageData();
   },error => {
      this.uiService.showError();
     // this.processing = false;
    })
  }


  exportExcel(){
    this.usersService.exportExcel().subscribe((resp : any) => {
      this.districtService.exportAsExcelFile(resp,'users.xlsx')
    })
  }

}
