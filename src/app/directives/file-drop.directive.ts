import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appFileDrop]'
})
export class FileDropDirective {

  @Output() filesDropped : EventEmitter<FileList> = new EventEmitter<FileList>();
  @Output() filesHovered  = new EventEmitter();
  @Input() multiple = true;
  constructor() { }

  @HostListener('drop',['$event'])
  onDrop($event){
    $event.preventDefault();
    let transfer = $event.dataTransfer;
    if(this.multiple){
      this.filesDropped.emit(transfer.files);
    }else{
      this.filesDropped.emit(transfer.files[0]);
    }
    this.filesHovered.emit(false);
  }

  @HostListener('dragover',['$event'])
  onDragOver($event){
    $event.preventDefault();
    this.filesHovered.emit(true);
  }

  @HostListener('dragleave',['$event'])
  onDragLeave($event){
    $event.preventDefault();
    this.filesHovered.emit(false);
  }

}
