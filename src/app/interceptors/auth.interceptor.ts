import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";
import {UiService} from "../services/ui.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
  constructor(private authService : AuthService,private router: Router,private uiService : UiService){}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const copiedRequest = req
      .clone({
        headers : req.headers.set('Authorization', `Bearer ${this.authService.token}`)
      });
    return next.handle(copiedRequest).pipe(
      catchError(error => {
        if(error.error && error.error.code  === 401){
          localStorage.clear();
          this.router.navigate(['/sign-in']).then(resp => {
            this.uiService.showError('Session Expired!')
          })
        }
        return throwError(error);
      })
    );
  }

}
