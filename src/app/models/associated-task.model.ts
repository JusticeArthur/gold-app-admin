import {Question} from "./question.model";

export interface AssociatedTask {
  id : number;
  task : string;
  question : Question;
}
