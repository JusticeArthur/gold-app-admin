import {QuestionResponse} from "./response.model";
import {AssociatedTask} from "./associated-task.model";

export interface Question {
  id : number;
  question : string;
  topic : {};
  question_type : string;
  strong_point : string;
  weak_point : string;
  weight : number;
  status : string;
  responses : QuestionResponse[];
  task : AssociatedTask;
}
