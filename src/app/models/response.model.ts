import {Question} from "./question.model";

export interface QuestionResponse {
  id : number;
  possible_response : string;
  correct : boolean;
  question : Question;
}
