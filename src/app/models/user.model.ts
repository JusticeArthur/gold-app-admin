import {Permission} from "./permission.model";
import {Role} from "./role.model";

export class User {
  constructor(firstName : string,lastName : string, email : string,
              dob : Date,username : string ,private _token : string,
              private _tokenExpiration : Date,
              verified : boolean,
              permissions : Permission[], roles : Role[]){}

              get token(){
                if(new Date(this._tokenExpiration ) > new Date())
                    return this._token;
                return null;
              }

              get isAuth(){
                  return !! this.token;
              }
}
