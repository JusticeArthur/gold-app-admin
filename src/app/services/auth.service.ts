import { Injectable } from '@angular/core';
import {EnvService} from "./env.service";
import {BehaviorSubject, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {catchError, tap} from "rxjs/operators";
import {Role} from "../models/role.model";
import {Permission} from "../models/permission.model";
import {User} from "../models/user.model";
import {Router} from "@angular/router";

export interface Country {
  name : string,
  code : string,

}
export interface Region {
  name :string,
  country : Country
}
export interface District {
  name : string,
  region : Region,
}

export interface Topic {
  name : string;
  id : number;
}

export interface Community {
  name : string,
  region : District,
}

export interface Pillar {
  id : number;
  name : string;
  description : string;
}

export interface topic {
  name : string,
  pillar_id : Pillar,
  description : string,
}

export interface Program {
  name : string,
  manager_id : User,
  code: string,
  description : string,
}

export interface UserDetails {
  address : string,
  dob : string,
  community : string,
  country : Country
  district : District
  region : Region,
  email : string,
  email_verified_at : string,
  first_name : string,
  last_name : string,
  username : string,
  gender : 'male' | 'female' | 'other',
  key : string,
  status : 'active' | 'inactive' | 'blocked',
}


export interface AuthResponse {
  token : string,
  token_expiry : string,
  user : {
    address : string,
    dob : string,
    community : string,
    country : Country
    district : District
    region : Region,
    email : string,
    email_verified_at : string,
    first_name : string,
    last_name : string,
    username : string,
    gender : 'male' | 'female' | 'other',
    key : string,
    status : 'active' | 'inactive' | 'blocked',
    roles : Role[],
    permissions : Permission[]
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _user = new BehaviorSubject<User>(null);
  constructor(private envService : EnvService,private httpClient : HttpClient,private router : Router) { }

  get user(){
    return this._user.asObservable();
  }

  setUser(user : User){
    this._user.next(user);
  }

  createNewUserInstance(){
    const resp = <AuthResponse>JSON.parse(localStorage.getItem('userInfo'));
    const user = resp.user;
    let incomingUser = new User(user.first_name,user.last_name,user.email,
      new Date(user.dob),user.username,resp.token,new Date(resp.token_expiry),
      !!user.email_verified_at,user.permissions,user.roles);
    this.setUser(incomingUser);
  }
  signIn(formData : FormData){
    return this.httpClient.post<{data : AuthResponse}>(`${this.envService.baseUrl}/sign-in`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      )
  }

  isAuthenticated(){
    if(localStorage.getItem('userInfo')){
      this.createNewUserInstance();
    }else {
      return false;
    }
    if(this._user.value){
      return this._user.value.isAuth;
    }
    return false;
  }

  autoSignIn(){
    if(localStorage.getItem('userInfo')){
      this.createNewUserInstance();
      this.router.navigate(['/dashboard'])
    }
  }




  get token(){
    if(this._user.value){
      return this._user.value.token;
    }
    return null;
  }
}
