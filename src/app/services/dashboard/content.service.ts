import {Injectable} from '@angular/core';
import {EnvService, Paginatable} from "../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Community, Topic} from "../auth.service";
import {Question} from "../../models/question.model";

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private envService: EnvService, private httpClient: HttpClient) {
  }

  get uploadQueUrl(){
    return `${this.envService.baseUrl}/dashboard/content/question`;
  }
  get pushUrl(){
    return `${this.envService.baseUrl}/dashboard/content/push-content`;
  }

  get pillarUrl(){
    return `${this.envService.baseUrl}/dashboard/settings/pillars`;
  }
  get miscEndpoint(){
    return `${this.envService.baseUrl}/dashboard/settings/misc`;
  }

  fetchFullQuestions(url = null){
    return this.httpClient.get<{data : Paginatable<Question>}>(
      `${url ? url : this.uploadQueUrl}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {
        })
      );
  }

  fetchQuestions(topicId,url = null){
    return this.httpClient.get<{data : {content : Paginatable<Question>, questions : Paginatable<Question>}}>(
      `${url ? url : this.uploadQueUrl}${url ? '&': '?'}${topicId > 0 ? `topic=${topicId}` : ''}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {
        })
      );
  }
  deleteQuestion(id,url = this.uploadQueUrl){
    return this.httpClient.delete(`${url}/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {
        })
      );
  }
  addQuestion(question: any) {
    return this.httpClient.post(this.uploadQueUrl, question)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {
        })
      );
  }
  addBulk(question: FormData) {
    return this.httpClient.post(this.uploadQueUrl, question)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {
        })
      );
  }
  pillarTopics(id){
    return this.httpClient.get<{data : Topic[]}>(`${this.miscEndpoint}/topics/${id}`).pipe(
      catchError(error => throwError(error)),
      tap(resp => resp)
    );
  }
  pushContent(formData : FormData){
    return this.httpClient.post(this.pushUrl,formData).pipe(
      catchError(error => throwError(error)),
      tap(resp => resp)
    )
  }



}

