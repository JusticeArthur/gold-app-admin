import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Community} from "../../auth.service";

@Injectable({
  providedIn: 'root'
})
export class CommunitiesService {

  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addCommunity(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/community`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchCommunity(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<Community>}>(`${this.envService.baseUrl}/dashboard/settings/community?size=${perPage}&search=${search}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Community>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateCommunity(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/community/${id}`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteCommunity(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/community/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-communities`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }
}
