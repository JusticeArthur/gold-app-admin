import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Program} from "../../auth.service";

@Injectable({
  providedIn: 'root'
})
export class EducationService {

  
  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addEducation(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/education`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchEducation(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<any>}>(`${this.envService.baseUrl}/dashboard/settings/education?size=${perPage}&search=${search}`)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Program>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateEducation(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/education/${id}`,formData)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteEducation(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/education/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-educations`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
}
}
