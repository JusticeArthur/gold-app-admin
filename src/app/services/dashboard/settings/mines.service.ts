import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, UserDetails, Community} from "../../auth.service";

@Injectable({
  providedIn: 'root'
})
export class MinesService {

 
  
  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addMine(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/mines`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchMines(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<UserDetails>}>(`${this.envService.baseUrl}/dashboard/settings/mines?size=${perPage}&search=${search}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<UserDetails>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateMine(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/mines/${id}`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteMine(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/mines/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchRegionAll(country : number){
    return this.httpClient.get<{data : District[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/districts/${country}`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}

fetchCommunity(country : number){
  return this.httpClient.get<{data : Community[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/community/${country}`)
  .pipe(
    catchError(error => throwError(error.error)),
    tap(resp => {})
  );
}

exportExcel(){
  return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-mines`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}
}
