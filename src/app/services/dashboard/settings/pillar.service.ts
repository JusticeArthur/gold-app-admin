import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Program} from "../../auth.service";
@Injectable({
  providedIn: 'root'
})
export class PillarService {

  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addPillars(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/pillars`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }
  get pillarUrl(){
    return `${this.envService.baseUrl}/dashboard/settings/pillars`
  }

  fetchPillars(perPage = 0 , search = ''){
    return this.httpClient.get<{data : Paginatable<any>}>(perPage > 0 ? `${this.envService.baseUrl}/dashboard/settings/pillars?size=${perPage}&search=${search}` : this.pillarUrl)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Program>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updatePillar(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/pillars/${id}`,formData)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deletePillar(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/pillars/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }


  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-pillars`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }
}
