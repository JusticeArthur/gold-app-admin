import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Program} from "../../auth.service";

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addProgram(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/programs`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchProgram(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<Program>}>(`${this.envService.baseUrl}/dashboard/settings/programs?size=${perPage}&search=${search}`)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Program>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateProgram(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/programs/${id}`,formData)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteProgram(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/programs/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchUsers(){
    return this.httpClient.get<{data : Region[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/get-users`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
  }

  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-programs`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
}

}
