import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region} from "../../auth.service";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  fetchRegions(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<Region>}>(`${this.envService.baseUrl}/dashboard/settings/regions?size=${perPage}&search=${search}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Region>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchCountries(){
    return this.httpClient.get<{data : Country[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/countries`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  addRegion(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/regions`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchRegionAll(country : number){
    return this.httpClient.get<{data : Region[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/regions/${country}`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}
  updateRegion(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/regions/${id}`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteRegion(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/regions/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-regions`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }


  fetchCount(){
    return this.httpClient.get<{data : any[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/counts`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}
}
