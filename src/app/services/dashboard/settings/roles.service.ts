import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, Program, Community} from "../../auth.service";

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addRole(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/roles`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchRole(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<Program>}>(`${this.envService.baseUrl}/dashboard/settings/roles?size=${perPage}&search=${search}`)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<Program>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateRole(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/roles/${id}`,formData)
     .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteRole(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/roles/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPermissions(){
    return this.httpClient.get<{data : any[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/permissions`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
  }


 
}
