import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, Pillar, topic} from "../../auth.service";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

 
  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  fetchTopics(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<topic>}>(`${this.envService.baseUrl}/dashboard/settings/topics?size=${perPage}&search=${search}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<topic>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPillars(pillar : number){
    return this.httpClient.get<{data : Pillar[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/pillars/${pillar}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchAllTopics(pillar : number){
    return this.httpClient.get<{data : topic[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/topics/${pillar}`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}


  addTopic(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/settings/topics`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchRegionAll(country : number){
    return this.httpClient.get<{data : Region[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/regions/${country}`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}
  updateTopic(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/settings/topics/${id}`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteTopic(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/topics/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  exportExcel(){
    return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-topics`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }
/*
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }*/
}
