import { Injectable } from '@angular/core';
import {EnvService, Paginatable} from "../env.service";
import {HttpClient} from "@angular/common/http";
import {catchError, debounceTime, tap} from "rxjs/operators";
import {throwError} from "rxjs";
import {Country, Region, District, UserDetails} from "../auth.service";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  
  constructor(private envService : EnvService,private httpClient : HttpClient) { }

  addUser(formData : FormData){
    return this.httpClient.post(`${this.envService.baseUrl}/dashboard/users`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchUsers(perPage : number , search = ''){
    return this.httpClient.get<{data : Paginatable<UserDetails>}>(`${this.envService.baseUrl}/dashboard/users?size=${perPage}&search=${search}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchPaginatedRecord(url : string){
    return this.httpClient.get<{data : Paginatable<UserDetails>}>(url)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  updateUser(formData,id){
    return this.httpClient.put(`${this.envService.baseUrl}/dashboard/users/${id}`,formData)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  deleteUser(id){
    return this.httpClient.delete(`${this.envService.baseUrl}/dashboard/settings/users/${id}`)
      .pipe(
        catchError(error => throwError(error.error)),
        tap(resp => {})
      );
  }

  fetchDistrict(country : number){
    return this.httpClient.get<{data : District[]}>(`${this.envService.baseUrl}/dashboard/settings/misc/districts/${country}`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}

exportExcel(){
  return this.httpClient.get(`${this.envService.baseUrl}/dashboard/settings/misc/excel-export-users`)
    .pipe(
      catchError(error => throwError(error.error)),
      tap(resp => {})
    );
}

}
