import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DeletionService {

  private _status = new BehaviorSubject<{data : any}>(null);
  constructor() { }

  get status(){
    return this._status.asObservable();
  }

  setStatus(data : any){
    this._status.next(data);
  }

}
