import { Injectable } from '@angular/core';


export interface Paginatable <T> {
  current_page : number,
  data : T[],
  first_page_url : string,
  from : number,
  last_page : number,
  last_page_url : string,
  next_page_url : string,
  path : string,
  per_page : number,
  prev_page_url : string,
  to : number,
  total : number
}
@Injectable({
  providedIn: 'root'
})

export class EnvService {

  baseUrl = 'http://localhost:8000/api/v1';
  constructor() { }
}
