import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {RegionService} from "./dashboard/settings/region.service";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor(private regionService : RegionService,config: NgbModalConfig, private modalService: NgbModal,private toastr : ToastrService) {
    config.keyboard = false;
  }

  showSuccess(message : string){
    this.toastr.success(message,'Success!')
  }

  showError(message = 'sorry an error occurred'){
    this.toastr.error(message,'Error!');
  }

  showWarning(message : string){
    this.toastr.warning(message,'Warning!');
  }

  openModal(content) {
    this.modalService.open(content);
  }

  closeModal(){
    this.modalService.dismissAll();
  }
}
