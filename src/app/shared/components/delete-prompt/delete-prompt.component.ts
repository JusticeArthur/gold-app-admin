import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {UiService} from "../../../services/ui.service";
import {DeletionService} from "../../../services/deletion.service";
import {fakeAsync} from "@angular/core/testing";

@Component({
  selector: 'app-delete-prompt',
  templateUrl: './delete-prompt.component.html',
  styleUrls: ['./delete-prompt.component.scss']
})
export class DeletePromptComponent implements OnInit {

  text = 'destroy';
  security = '';
  @ViewChild('inputElement',{static : false}) inputElement : ElementRef<HTMLInputElement>;
  @Output() shown : EventEmitter<string> = new EventEmitter<string>();
  constructor(private uiService : UiService,private deleteService : DeletionService) { }

  ngOnInit() {
    this.shown.subscribe(resp => {
      this.inputElement.nativeElement.focus();
    })
  }

  closeModal(){
    this.uiService.closeModal();
  }

  deleteConfirmed(){
    this.deleteService.setStatus({status : 'confirmed'});
    this.uiService.closeModal();
  }

}
