import { NgModule } from '@angular/core';
import { LoaderComponent } from './components/loader/loader.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {NgxSelectModule} from "ngx-select-ex";
import { DeletePromptComponent } from './components/delete-prompt/delete-prompt.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";

import { ArchwizardModule } from 'angular-archwizard';
import {MatCardModule} from "@angular/material/card";
import {DragDropModule} from "@angular/cdk/drag-drop";
//import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [LoaderComponent, DeletePromptComponent],
  imports : [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    NgxSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ArchwizardModule,
    MatCardModule,
    DragDropModule
  //  BsDatepickerModule.forRoot()
  ],
  exports: [
    LoaderComponent,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    NgxSelectModule,
    DeletePromptComponent,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ArchwizardModule,
    DeletePromptComponent,
    MatCardModule,
    DragDropModule
  ],

})
export class SharedModule { }
